defmodule Shopping do
	def get_input(map \\%{}) do

		input_string = IO.gets("enter the query\n")
		               |> String.trim
		split_string = String.split(input_string, "=>")
		operation = hd(split_string)
		details = Enum.at(split_string, 1)

		case operation do
			"INVENTORY" -> inventory(map, details)

			"STOCK" ->
				if(details != nil) do
					stock(map, details)
				else
					stock(map)					
				end
			"SELL" ->
				sell(map)

			_ ->
				IO.puts("See ya")		
		end
	end

	def inventory(map, details) do
		[item | other_details] = String.split(details, ",")
		if Map.has_key?(map, item) do
			numberized_other_details = Enum.map(other_details, fn(x) -> String.to_integer(x) end)
			quantity = Enum.at(numberized_other_details, 0)
			show_map = map[item]
			new_show_map = Map.update!(show_map, "quantity", &(&1+quantity))
			new_map = Map.update!(map, item, fn(_x) -> new_show_map end)
			IO.puts("Item updated")
			stock(new_map)
		else
			numberized_other_details = Enum.map(other_details, fn(x) -> String.to_integer(x) end)
			titles = ["quantity", "price"]
			new_item_details = Enum.zip(titles, numberized_other_details) |> Enum.into(%{})
			new_map = Map.merge(map, %{item => new_item_details})
			IO.puts("Item Succesfully added")
			IO.inspect(new_map)
			stock(new_map)
		end	
	end

	def stock(map, item) do
		if Map.has_key?(map, item) do
			show_map = map[item]
			keys = Map.keys(show_map)
			Enum.each(keys, fn(x) -> IO.puts("#{x}=>#{show_map[x]}") end)	
		else
			IO.puts("No such item\n")
		end
		get_input(map)	
	end

	def stock(map) do
		main_keys = Map.keys(map)
		Enum.each(main_keys,fn(x) ->
			IO.puts(x)
			side_map = map[x]
			side_keys = Map.keys(side_map)
			Enum.each(side_keys, fn(details) -> IO.puts("#{details}=>#{side_map[details]}") end)
		end
			)
		get_input(map)
	end

	def sell(map, sale_map \\ %{}) do
		item = IO.gets("Enter the item\n") |> String.trim
		if Map.has_key?(map, item) do
			sell_quantity = IO.gets("Enter the required quanity\n") 
							|> String.trim 
							|> String.to_integer()
			quantity = map[item]["quantity"]
			if(sell_quantity > quantity) do
				IO.puts("Required quantity exceeds existing quantity")
				stock(map, item)
			end				
			show_map = map[item]
			new_show_map = Map.update!(show_map, "quantity", &(&1 - sell_quantity))
			new_map = Map.update!(map, item, fn(_x) -> new_show_map end)
			sale_details = ["quantity", "price"]
			sale_done = [sell_quantity, map[item]["price"]]
			product_map = Enum.zip(sale_details, sale_done) |> Enum.into(%{})
			new_sale_map = Map.merge(sale_map, %{item => product_map})

			opinion = IO.gets("Want to make another sale, yes or no\n") |>String.trim

			case opinion do
				"yes" -> 
					sell(new_map, new_sale_map)
				"no" ->
					bill(new_sale_map)
					get_input(new_map)	
			end
		end
	end

	defp bill(sale_map) do
		amount=0
		totalamount = 0
		keys = Map.keys(sale_map)
		Enum.each(keys, fn(item) ->
			quantity = sale_map[item]["quantity"]
			price = quantity * sale_map[item]["price"]
			IO.puts("#{item} #{quantity} #{price}\n")
			totalamount = amount + price
			amount = totalamount
		end
			)
		IO.puts("TOTAL: #{amount}")
	end
end