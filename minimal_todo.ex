defmodule MinimalTodo do
	def getfilename do
		filename = IO.gets("Enter the csv filename\n") |> String.trim
		read(filename)
			|> parse
			|> get_command
			    
	end

	def get_command(data) do
		command = IO.gets(""" 
					Type the first Letter of command
					R)ead Todo A)dd Todo D)elete Todo L)oad csv S)ave csv
					""")
		|> String.trim
		|> String.downcase

		case command do
			"r" -> show_todo(data)
			"d" -> delete_todo(data)
			"a" -> add_todo(data)
 			"q" -> IO.puts("Goodbye")
			_   -> IO.puts("Give a valid input\n")
				   get_command(data)	 
		end
	end


	def read(filename) do
		case File.read(filename) do
			{:ok, body} -> 
				body
			{:error, reason} ->
				IO.puts(~s(Cannot open file "#{filename}"\n))
				IO.puts(~s("#{:file.format_error reason}\n"))
				getfilename()
		end
	end

	def parse(body) do
		[header|lines] = String.split(body, ~r{(\n)}) |> Enum.filter(fn(x) -> x != "" end)
		titles = tl(String.split(header, ","))
		parse_lines(lines,titles)
	end

	def parse_lines(lines,titles) do
		Enum.reduce(lines, %{}, fn(line,built) ->
			[name|fields] = String.split(line,",")
			if(Enum.count(fields) == Enum.count(titles)) do
				line_data=Enum.zip(titles,fields) |> Enum.into(%{})
				Map.merge(built, %{name => line_data})
			else
				built
			end	
		end)
	end

	def show_todo(data, next_command? \\ true) do
		items = Map.keys(data)
		IO.puts("You have the following todos\n")
		Enum.each(items, fn(item) -> IO.puts(item) end)
		if next_command? do
			get_command(data)	
		end
	end

	def delete_todo(data) do
		todo = IO.gets("Enter the todo to be deleted\n") |> String.trim
		if Map.has_key?(data, todo) do
			IO.puts("OK")
			new_map = Map.drop(data,[todo])	
			IO.puts(~s("#{todo}" has been deleted\n))
			show_todo(new_map, false)
			get_command(data)
		else
			IO.puts(~s(There is no todo named #{todo}!\n))
			show_todo(data)
			delete_todo(data)	
		end
	end

	def add_todo(data) do
		[name|lines] = IO.gets("Enter the todo with the priority,urgency,date added and Notes seperated by ,\n")  |> String.trim
				|> String.trim
				|> String.split(",")
				|> Enum.filter(fn(x) -> x != "" end)
		titles = ["priority", "urgency", "date added", "Notes"]
		line_data = Enum.zip(titles,lines) |> Enum.into(%{})		 
		new_map = Map.merge(data,%{name => line_data})
		show_todo(new_map,false)
		get_command(new_map)
	end
end