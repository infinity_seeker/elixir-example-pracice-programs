defmodule WordCount do
	def getfilename do
		filecontrol = IO.gets(~s(Enter the file name and operation of type "h" for help\n)) |>String.trim
		operation(filecontrol)
	end
	def operation(filecontrol) do
		if filecontrol == "h" do
			IO.puts("""
				Usage [filename][flags]
				flags:
				-l count no of lines
				-w count no of words (default)
				-c count no of charecters
				""")
			getfilename()
		else 
			parts = String.split(filecontrol,~r{ -})
			filename = Enum.at(parts,0)
			flag = Enum.at(parts,1)
			body=File.read!(filename)
			case flag do
				"w" ->
					words = String.split(body, ~r{(\\n|[^\w'])+}) |> Enum.filter(fn(x) -> x != "" end)
					IO.puts(Enum.count(words))
				"l" ->
					lines = String.split(body, ~r{(\n\r|\n|\r)})
					IO.puts(Enum.count(lines))
				"c" ->
					charecters = String.split(body, "")
					IO.puts(Enum.count(charecters))
				nil ->
					words = String.split(body, ~r{(\\n|[^\w'])+}) |> Enum.filter(fn(x) -> x != "" end)
					IO.puts(Enum.count(words))							
			end
		end
	end	
end