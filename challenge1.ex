defmodule Challenge1 do
	def getname do
		name = IO.gets("Please Enter a name or Enter stop\n")
		greet(name)
	end

	def greet(name) do
		case String.trim(name) do
			"stop" -> IO.puts("Nice meeting you")
			"minat" -> IO.puts("Hola you entered Minat's name\n")
					   getname()	
			_ -> IO.puts("Welcome #{name}\n")
				 getname() 
		end
	end
end