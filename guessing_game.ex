defmodule GuessingGame do

	def guess(a,b) when a > b, do: guess(b,a)

	def guess(low, high) do
		response = IO.gets("Maybe You were thinking of #{mid(high,low)}\n")
		case String.trim(response) do
			"bigger" ->
				bigger(low,high)
			"smaller" ->
				smaller(low,high)	
			"yes" ->
				IO.puts("I Knew i could do it\n")
			 _ ->
			 	IO.puts("Please Enter bigger Smaller\n")
			 	guess(low,high)		
		end
	end

	def mid(low, high) do
		div(low+high,2)
	end

	def bigger(low,high) do
		new_low = mid(low,high)+1
		guess(new_low, high)
	end

	def smaller(low,high) do
		new_high = mid(low,high)-1
		guess(low, new_high)
	end	
end