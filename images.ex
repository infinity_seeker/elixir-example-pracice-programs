{:ok, files} = File.ls(".")
imagefiles = Enum.filter(files, fn(x) -> Regex.match?(~r/\.jpg/,x) end)

imagecount = Enum.count(imagefiles)

case imagecount do
	1 -> match_word = "file"
	_ -> match_word = "files"

IO.puts("found #{imagecount} #{match_word}")	

case File.mkdir('./images') do
	{:ok} -> IO.puts("Images Directory created\n")
	{:error, _} -> IO.puts("Images directory cannot be created\n")
end	

Enum.each(imagefiles, fn(filename) -> File.rename(filename, "./images/#{filename}") end)


end