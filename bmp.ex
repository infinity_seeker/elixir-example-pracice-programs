defmodule BMP do
	def file_header(offset \\ 26) do
		file_type = "BM"
		file_size = <<0::little-size(32)>> #zero for uncompresed
		reserved = <<0::little-size(32)>> #always zero
		bitmap_offset = <<offset::little-size(32)>> #number of bytes before image data
		file_type <> file_size <> reserved <> bitmap_offset
	end

	def wi2x_header(width, height, bits_per_pixel \\24) do
		size = <<12::little-size(32)>>
		w = <<width::little-size(16)>>
		h = <<height::little-size(16)>>
		planes = <<1::little-size(16)>>
		bpp = <<bits_per_pixel::little-size(16)>>
		size<>w<>h<>planes<>bpp
	end

	def example_data(width, height) do
		for row <- 1..height, into: <<>> do
			bytes_past = rem(3 * width, 4)
			padding = if(bytes_past > 0) do 
					  	(4 - bytes_past) * 8 
					  else
					    0
					  end   
			for col <- 1..width, into: <<>> do
				<<(100 + 5 * col)::little-size(8),
				  (2 * row)::little-size(8),
				  (5 * col + row)::little-size(8),
				  0::size(padding)>>
			end
		end
	end

	def save_file(filename, header, pixels) do
		File.write!(filename, file_header() <> header <> pixels)
	end

	def example_file(width \\ 200, height \\ 300, name \\ "hello.bmp") do
		save_file(name, wi2x_header(width, height), example_data(width, height))
	end
end