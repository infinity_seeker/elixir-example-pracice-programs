defmodule Greeting do
	def getname do
		name = IO.gets("Enter a name or type stop\n") |> String.trim
		if(name==="stop") do
			endit()
		else	
			country = IO.gets("Enter you country\n") |> String.trim
			greet(name,country)
		end		
	end 

	def greet(name,country) do
		cond do
			country === "america" ->
				IO.puts("Hello #{name}")
				getname()
			country === "spain" ->
				IO.puts("Hola #{name}")
				getname()
			country === "french" ->
				IO.puts("Bonjour #{name}")	
				getname()
		end
	end

	def endit do
		IO.puts("Well then goodbye for now")
	end	
end