defmodule Ch14 do

	def squish([]), do: []
	def squish([head|tail]), do: squish(head) ++ squish(tail)
	def squish(head), do: [head]


	def squash(list), do: squash(list, []) |> Enum.reverse
	def squash([], acc), do: acc
	def squash([[]|tail], acc), do: squash(tail, acc)
	def squash([head|tail], acc) do: squash(tail, [head | acc])
	def squash([head|tail]) when is_list(head) do
		squash(tail, squash(head, acc))
	end 

end	