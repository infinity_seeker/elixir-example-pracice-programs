defmodule Factorial do

	def get_input do
		n = IO.gets("Enter the number\n") |> String.trim |> String.to_integer
		factorial(1, n)
	end

	def factorial(prod, n) when n != 1 do
		new_prod = prod * n
		new_num = n - 1
		factorial(new_prod, new_num)
	end

	def factorial(prod, _) do
		IO.puts("The factorial of given number is #{prod}\n")
	end	

end