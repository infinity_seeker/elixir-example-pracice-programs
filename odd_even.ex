defmodule OddEven do
	def getnum do
		number = IO.gets("Please Enter a number\n") |> String.trim |> String.to_integer
		check(number) 
	end
	def check(number) do
		if(rem(number,2)===1) do
			IO.puts("The number is odd\n")
		else
			IO.puts("The number is even\n")	
		end
	end
end