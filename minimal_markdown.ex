defmodule MinimalMakdown do
	
	def tohtml() do
		text = IO.gets("Enter a Text\n")
		|> String.trim
		|> bold
		|> italics
		|> small
		|> big

		File.write("out.html", text)
	end

	def italics(text) do
		Regex.replace(~r/\*(.*)\*/, text, "<em>\\1</em>")
	end

	def bold(text) do
		Regex.replace(~r/\*\*(.*)\*\*/, text, "<b>\\1</b>")
	end

	def p(text) do
		Regex.replace(~r/(\r\n|\r|\n|^)+([^\r\n]+)((\r\n|\r|\n)+$)?/, text, "<p>//2</p>")
	end

	def big(text) do
		Regex.replace(~r/\#(.*)\#/, text, "<big>\\1</big>")
	end

	def small(text) do
		Regex.replace(~r/\#\#(.*)\#\#/, text, "<small>\\1</small>")
	end

end