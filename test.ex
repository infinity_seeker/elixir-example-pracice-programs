defmodule Test do
	
	def getfilename do
	    filename = IO.gets("Enter the file name\n") |> String.trim	
	    read(filename)
	end

	def read(filename) do
		body = File.read!(filename)
		words = String.split(body, "")	
		IO.inspect(words)
	end

end